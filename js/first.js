$(document).ready(function(e) {
	$('.content_review_evaluation_stars').mousemove(function(){
			$(this).children().each(function(index, element) {
                $(this).css('background','url(img/star.png)');
            });
		})
	
	$('.content_review_evaluation_stars').click(function(){
			var count_stars = $(this).attr('data-parameter');
			$('#count_stars').val(count_stars);
		})
		
	$('.content_review_evaluation_stars').mouseout(function(){
			$(this).children().each(function(index, element) {
                $(this).css('background','url(img/star_transparent.png)');
            });
		})
		
	$('input[name="delivery"]').change(function(){
			if($('#courier').prop("checked"))
			{
				$('.delivery_div_courier').css('display','block');
				$('.delivery_div_pickup').css('display','none');
			}
			else
			{
				$('.delivery_div_courier').css('display','none');
				$('.delivery_div_pickup').css('display','block');
				if($('#street').val() == '')
					{$('#street').val('--');}
				if($('#house').val() == '')
					{$('#house').val('--');}
				if($('#flat').val() == '')
					{$('#flat').val('--');}
			}			
		})
});