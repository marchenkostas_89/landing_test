-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 21 2016 г., 20:42
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `internet`
--

DELIMITER $$
--
-- Процедуры
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_order`(IN `pproduct_id` INT(11), IN `pclient_id` INT(11), IN `pamount` INT(11), IN `pdate_` DATETIME)
Begin
declare pprice float;
set pprice = (select price from prices where product_id = pproduct_id);
insert into orders(client_id, product_id, amount, cost, date_) VALUES
					(pclient_id, pproduct_id, pamount, pamount*pprice, pdate_);
update products_amount set amount = amount-1 where product_id = pproduct_id;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `categories_relations`
--

CREATE TABLE IF NOT EXISTS `categories_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_category_id` int(11) NOT NULL,
  `child_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_category_id` (`parent_category_id`),
  KEY `child_category_id` (`child_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AVG_ROW_LENGTH=5461 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `categories_relations`
--

INSERT INTO `categories_relations` (`id`, `parent_category_id`, `child_category_id`) VALUES
(1, 1, 2),
(2, 2, 3),
(3, 2, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AVG_ROW_LENGTH=4096 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name_`) VALUES
(1, 'Одежда'),
(2, 'Детская одежда'),
(3, 'Комплекты'),
(4, 'Колготки и носки');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_` varchar(200) NOT NULL,
  `login` varchar(20) NOT NULL,
  `pwd` varchar(20) NOT NULL,
  `phone` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AVG_ROW_LENGTH=8192 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id`, `name_`, `login`, `pwd`, `phone`) VALUES
(1, 'Иванов Иван', 'ivanov', '123', NULL),
(2, 'Коробкова Анастасия', 'nastya', '159753', '4692378');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `cost` float(9,3) NOT NULL,
  `date_` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `client_id` (`client_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AVG_ROW_LENGTH=5461 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `client_id`, `product_id`, `amount`, `cost`, `date_`) VALUES
(2, 2, 1, 2, 200.000, '2016-12-18 21:41:56'),
(3, 1, 2, 3, 66.000, '2016-12-18 21:41:56');

-- --------------------------------------------------------

--
-- Структура таблицы `prices`
--

CREATE TABLE IF NOT EXISTS `prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `price` float(9,3) NOT NULL,
  `date_changed` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AVG_ROW_LENGTH=8192 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `prices`
--

INSERT INTO `prices` (`id`, `product_id`, `price`, `date_changed`) VALUES
(1, 1, 100.000, '2016-12-18 21:33:28'),
(2, 2, 22.000, '2016-12-18 21:33:28');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_` varchar(100) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AVG_ROW_LENGTH=8192 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `name_`, `description`, `category_id`) VALUES
(1, 'Леггинсы 6мес', 'Детские леггинсы для девочек 6 месяцев', 4),
(2, 'Пижама с жирафом', 'Пижама из хлопка для мальчиков 4-5 лет', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `products_amount`
--

CREATE TABLE IF NOT EXISTS `products_amount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AVG_ROW_LENGTH=8192 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `products_amount`
--

INSERT INTO `products_amount` (`id`, `product_id`, `amount`) VALUES
(1, 1, 19),
(2, 2, 11);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `categories_relations`
--
ALTER TABLE `categories_relations`
  ADD CONSTRAINT `categories_relations_fk1` FOREIGN KEY (`parent_category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categories_relations_fk2` FOREIGN KEY (`child_category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_fk1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_fk2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `prices`
--
ALTER TABLE `prices`
  ADD CONSTRAINT `prices_fk1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_fk1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `products_amount`
--
ALTER TABLE `products_amount`
  ADD CONSTRAINT `products_amount_fk1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
