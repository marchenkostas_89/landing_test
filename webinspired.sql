-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 21 2016 г., 21:19
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `webinspired`
--

-- --------------------------------------------------------

--
-- Структура таблицы `form`
--

CREATE TABLE IF NOT EXISTS `form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `info` text NOT NULL,
  `info_lorem` text NOT NULL,
  `radio_test` varchar(255) NOT NULL,
  `checkbox_test` varchar(255) NOT NULL,
  `delivery` varchar(30) NOT NULL,
  `street` text NOT NULL,
  `house` int(4) NOT NULL,
  `access` int(2) NOT NULL,
  `intercom` int(3) NOT NULL,
  `floor` int(2) NOT NULL,
  `flat` int(3) NOT NULL,
  `housing` int(2) NOT NULL,
  `amazing` text NOT NULL,
  `pickup_select` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `menu_1`
--

CREATE TABLE IF NOT EXISTS `menu_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `menu_1`
--

INSERT INTO `menu_1` (`id`, `name`, `parent`) VALUES
(1, 'Блютуз наушники', 0),
(2, 'Микронаушники', 0),
(3, 'Блютуз колонки', 0),
(4, 'Аксессуары', 0),
(5, 'Apple', 2),
(6, 'Samsung', 2),
(7, 'Nokia', 2),
(8, 'Австрийское качество', 4),
(9, 'Немецкое качество', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `menu_2`
--

CREATE TABLE IF NOT EXISTS `menu_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `menu_2`
--

INSERT INTO `menu_2` (`id`, `name`, `parent`) VALUES
(1, 'Доставка и Оплата', 0),
(2, 'Гарантия', 0),
(3, 'О нас', 0),
(4, 'Контакты', 0),
(5, '1 год', 2),
(6, '2 года', 2),
(7, '3 года', 2),
(8, 'Информация', 3),
(9, 'Контакты', 3),
(10, 'Написать нам', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` varchar(20) NOT NULL,
  `stars` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `comment`, `name`, `date`, `stars`) VALUES
(1, 'Не ожидал что качество будет настолько хорошей для реплики! Спасибо за оперативную доставку! Мои рекомендации!', 'Анатолий', '19.12.2015', 5),
(2, 'Очень стильные и качественные!!звук отменный!!Спасибо за оперативную доставку!', 'Мария Сысоева', '1.11.2015', 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
